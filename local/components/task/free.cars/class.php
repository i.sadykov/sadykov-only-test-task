<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Iblock\Elements\ElementCarsForEmployeersTable;

class CFreeCars extends CBitrixComponent
{
    /**
     * Идентификатор highload-блока "Автомобили"
     *
     * @var int
     */
    public $carsHBId = 6;

    /**
     * Идентификатор highload-блока "Должности сотрудников и категории автомобилей"
     *
     * @var int
     */
    public $positionCarCategoryHBId = 8;

    /**
     * Идентификатор инфоблока "Автомобили для сотрудников" - инфоблок,
     * где размещается актуальная информация о доступных для конкретного сотрудника автомобилях
     * на запланированное время поездки
     *
     * @var int
     */
    public $recordsIblockId = 1;

    public function onPrepareComponentParams($arParams)
    {
        global $USER;
        return [ 'USER_ID' => $arParams['USER_ID'] ? intval($arParams['USER_ID']) : $USER->GetID() ];
    }

    public function executeComponent()
    {
        $request = Application::getInstance()->getContext()->getRequest()->getValues();

        // дата и время в формате - 2024-06-04 10:30:00

        if (
            isset($request['start_datetime']) && !empty($request['start_datetime'])
            && isset($request['end_datetime']) && !empty($request['end_datetime'])
        ) {
            Loader::includeModule('iblock');

            $userPosition = $this->getUserPosition($this->arParams['USER_ID']);
            if ($userPosition) {
                $userCarCategories = $this->getUserCarCategories($userPosition);
                if (!empty($userCarCategories)) {
                    $cars = $this->getCars([
                        'carCategories' => $userCarCategories,
                        'modelName' => $request['model'] ?? '',
                        'categoryName' => $request['category'] ?? '',
                        'driverName' => $request['driver'] ?? ''
                    ]);

                    if (!empty($cars)) {

                        $records = ElementCarsForEmployeersTable::getList([
                            'filter' => [
                                'ACTIVE' => 'Y',
                                '@USER_ID.VALUE' => $this->arParams['USER_ID'],
                                'CAR.ID' => $cars,
                                '>=TRIP_START_DATETIME.VALUE' => $request['start_datetime'],
                                '<TRIP_START_DATETIME.VALUE' => $request['end_datetime'],
                            ],
                            'select' => [
                                'ID',
                                'NAME',

                                'CAR.ID',
                                'CAR.UF_NAME',
                                'CAR.UF_DRIVER',
                                'CAR.UF_CAR_CATEGORY',

                                'CATEGORY.UF_NAME',

                                'DRIVER.UF_NAME',

                                //'CAR',
                                //'CATEGORY',
                                //'DRIVER',

                                'TRIP_START_DATETIME',
                                'TRIP_END_DATETIME',
                            ],
                            'runtime' => [
                                new ReferenceField(
                                    'CAR',
                                    '\Cars',
                                    [ '=this.CAR_ID.VALUE' => 'ref.UF_NAME' ],
                                    [ 'join_type' => 'LEFT' ]
                                ),
                                new ReferenceField(
                                    'CATEGORY',
                                    '\CarCategories',
                                    [ '=this.CAR.UF_CAR_CATEGORY' => 'ref.ID' ],
                                    [ 'join_type' => 'LEFT' ]
                                ),
                                new ReferenceField(
                                    'DRIVER',
                                    '\Drivers',
                                    [ '=this.CAR.UF_DRIVER' => 'ref.ID' ],
                                    [ 'join_type' => 'LEFT' ]
                                )
                            ]
                        ])->fetchAll();

                        $records ? $this->arResult['ITEMS'] = $records : $this->arResult['MESSAGE'] = Loc::getMessage('EMPTY');
                    } else {
                        $this->arResult['MESSAGE'] = Loc::getMessage('USER_CARS_EMPTY');
                    }
                } else {
                    $this->arResult['MESSAGE'] = Loc::getMessage('USER_CAR_CATEGORIES_EMPTY');
                }
            } else {
                $this->arResult['MESSAGE'] = Loc::getMessage('USER_POSITION_EMPTY');
            }

        } else {
            $this->arResult['MESSAGE'] = Loc::getMessage('NOT_ENOUGH_PARAMETERS');
        }

        $this->includeComponentTemplate();
    }

    /**
     * Получение должности сотрудника
     *
     * @param $userId
     * @return mixed|void
     */
    public function getUserPosition($userId)
    {
        try {
            $rsUser = CUser::GetByID($userId);
            if ($rsUser) {
                $position = $rsUser->Fetch()['UF_POSITION'];
                return $position;
            }
        } catch (SystemException $e) {
            //$e->getMessage();
        }
    }

    /**
     * Получение категории комфорта автомобилей для текущего пользователя в зависимости от его должности
     *
     * @param $userPosition
     * @return array
     */
    public function getUserCarCategories($userPosition) {
        $categories = [];

        try {
            $hlblock = HLBT::getById($this->positionCarCategoryHBId)->fetch();
            $entity = HLBT::compileEntity($hlblock);
            $entityClass = $entity->getDataClass();

            $rsData = $entityClass::getList([
                'select' => [ 'UF_CAR_CATEGORY' ],
                'filter' => [ 'UF_POSITION' => $userPosition ],
                'cache' => [ 'ttl' => 3600 ]
            ]);

            while ($arData = $rsData->Fetch()){
                $categories[] = $arData['UF_CAR_CATEGORY'];
            }

        } catch (SystemException $e) {
            //$e->getMessage();
        }

        return $categories;
    }

    /**
     * Получение автомобилей доступных для текущего пользвателю
     *
     * @param $params
     * @return array
     */
    public function getCars($params) {
        $cars = [];
        try {
            $hlblock = HLBT::getById($this->carsHBId)->fetch();
            $entity = HLBT::compileEntity($hlblock);
            $entityClass = $entity->getDataClass();

            $select = [ '*' ];
            $filter = [];
            $runtime = [];

            /* по модели */
            if (isset($params['modelName']) && !empty($params['modelName'])) $filter = [ 'UF_NAME' => $params['modelName'] ];

            /* по категории комфорта */
            if (isset($params['categoryName']) && !empty($params['categoryName'])) {
                $select = array_merge($select, [ 'CAR_CATEGORY' ]);
                $filter = array_merge($filter, [
                    'LOGIC' => 'AND',
                    [ 'UF_CAR_CATEGORY' => $params['carCategories'] ],
                    [ 'CARS_CAR_CATEGORY_UF_NAME' => $params['categoryName'] ]
                ]);
                $runtime[] = new ReferenceField(
                    'CAR_CATEGORY',
                    '\CarCategories',
                    [ '=this.UF_CAR_CATEGORY' => 'ref.ID' ]
                );
            } else {
                $filter = array_merge($filter, [ 'UF_CAR_CATEGORY' => $params['carCategories'] ]);
            }

            /* по водителю */
            if (isset($params['driverName']) && !empty($params['driverName'])) {
                $select = array_merge($select, [ 'DRIVER' ]);
                $filter = array_merge($filter, [ 'CARS_DRIVER_UF_NAME' => $params['driverName'] ]);
                $runtime[] = new ReferenceField(
                    'DRIVER',
                    '\Drivers',
                    [ '=this.UF_DRIVER' => 'ref.ID' ]
                );
            }

            $rsData = $entityClass::getList([
                'select' => $select,
                'filter' => $filter,
                'runtime' => $runtime,
            ]);

            while ($arData = $rsData->Fetch()) {
                $cars[] = $arData['ID'];
            }
        } catch (SystemException $e) {
            //$e->getMessage();
        }

        return $cars;
    }

}

?>
