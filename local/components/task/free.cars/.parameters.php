<?php

use Bitrix\Main\Localization\Loc;

global $USER;

$arComponentParameters = [
    'PARAMETERS' => [
        'USER_ID' => [
            'NAME'      => Loc::getMessage('USER_ID'),
            'TYPE'      => 'STRING',
            'DEFAULT'   => $USER->GetID()
        ],
    ],
];

?>