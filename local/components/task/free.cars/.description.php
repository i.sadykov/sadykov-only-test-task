<?php

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
    'NAME'          => Loc::getMessage('COMPONENT_NAME'),
    'DESCRIPTION'   => Loc::getMessage('COMPONENT_DESC'),
    'PATH'          => [
        'ID'        => 'task',
        'NAME'      => Loc::getMessage('COMPONENT_PATH_NAME'),
    ]
];
?>